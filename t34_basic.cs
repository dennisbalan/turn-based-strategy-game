﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class t34_basic : MonoBehaviour, IMoveVelocity
{
    // Start is called before the first frame update
    private Vector3 velocityVector;
    private Rigidbody2D rigidbody2D;
    
    private void Awake()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    public void setVelocity(Vector3 velocityVector){
        this.velocityVector = velocityVector;
    }

    private void FixedUpdate()
    {
        rigidbody2D.velocity = velocityVector;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

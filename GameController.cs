﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;

public class GameController : MonoBehaviour
{
    
    private Vector3 startPosition;
    private List<UnitRTS> selectedUnitList;
    private void Awake()
    {
        selectedUnitList = new List<UnitRTS>();

    }
   
    private void Update()
    {   
        if (Input.GetMouseButtonDown(0))
        {
            startPosition = UtilsClass.GetMouseWorldPosition();

        }
        if (Input.GetMouseButtonUp(0))
        {
            Debug.Log(UtilsClass.GetMouseWorldPosition());
            Collider2D[] collider2Darray = Physics2D.OverlapAreaAll(startPosition, UtilsClass.GetMouseWorldPosition());
            foreach (UnitRTS unitRTS in selectedUnitList)
            {
                unitRTS.SetSelectedVisible(true);
            }
            selectedUnitList.Clear();
            foreach (Collider2D collider2D in collider2Darray)
            {
                UnitRTS unitRTS = collider2D.GetComponent<UnitRTS>();
                Debug.Log(collider2D);
                if(unitRTS != null)
                {
                    unitRTS.SetSelectedVisible(true);
                    selectedUnitList.Add(unitRTS);
                }
            }
            Debug.Log(selectedUnitList.Count);
        }
    }
}

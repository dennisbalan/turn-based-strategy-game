﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitRTS : MonoBehaviour
{
    // Start is called before the first frame update
    
        private GameObject selectedGameObject;
        
        private void Awake()
        {
            selectedGameObject = transform.Find("Selected").gameObject;
    
            SetSelectedVisible(false);
        }
    public void SetSelectedVisible(bool visible)
    {
        selectedGameObject.SetActive(visible);
    }
    
    public void moveUnit(Vector3 target)
    {

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}

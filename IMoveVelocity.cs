using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMoveVelocity 
{
    void setVelocity(Vector3 velocityVector);
}

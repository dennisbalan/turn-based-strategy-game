﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class T34MovePosition : MonoBehaviour
{
    
    private Vector3 movePosition;
    public void setMovePosition(Vector3 movePosition)
    {
        this.movePosition = movePosition;
    }
    private void Update()
    {
        Vector3 movedir = (movePosition - transform.position).normalized;
        GetComponent<t34_basic>().setVelocity(movedir);
    }

}
